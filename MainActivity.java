package com.example.webviewdemo;

import android.R.style;
import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		Dialog dialog = new Dialog(MainActivity.this);
		dialog.getContext().getTheme().applyStyle(style.Theme_NoTitleBar, true);

		TextView tv = new TextView(MainActivity.this);
		tv.setText("Dialog ������Ϣ");

		dialog.setContentView(tv);
		dialog.show();
	}

}
